import { Component, OnInit } from '@angular/core';
import { EndpointService } from '../services/endpoint.service';
import Lamp from '../models/Lamp';
import { PlaceType } from '../enums/PlaceType';
import TrafficSensor from '../models/TrafficSensor';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public posX: number;
  public posY: number;
  public placeType: PlaceType = PlaceType.NormalTraffic;
  public isEnabled: boolean;
  public wattPower: number;

  public lamp: Lamp = {
    TrafficSensor: {} as TrafficSensor,
  } as Lamp;

  constructor(private endpointService: EndpointService) { }

  ngOnInit() {
  }

  registerLamp() {
    this.lamp = {
      Power: 100,
      ConditionalPower: 100,
      Enabled: this.isEnabled,
      Place: this.placeType,
      PosX: this.posX,
      PosY: this.posY,
      WattPower: this.wattPower,
      TrafficSensor: {
        Range: 150,
        ObjectCount: 0,
      } as TrafficSensor
    } as Lamp;

    this.endpointService.registerLamp(this.lamp);
  }

  startSensor() {
    setInterval(() => {
      this.lamp.TrafficSensor.ObjectCount = Math.floor(Math.random() * 9) + 1;
      this.endpointService.updateLamp(this.lamp);
    },
    1000);
  }

}
