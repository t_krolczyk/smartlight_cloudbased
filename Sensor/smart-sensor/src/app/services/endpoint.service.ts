import { Injectable } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import Lamp from '../models/Lamp';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {

  public connection: HubConnection;

  constructor() {
    this.connection = new HubConnectionBuilder()
    //.withUrl('https://localhost:44337/sensor-hub')
    .withUrl('https://smartlight20190606015032.azurewebsites.net:443/sensor-hub')
    .build();

    this.startListening();
  }

  private startListening() {

    this.connection.on('RegisterLampResponse', (signal) => {
      console.log(signal);
    });

    this.connection.on('UpdateLampResponse', (signal) => {
      console.log(signal);
    });

    this.connection.start();
  }

  public registerLamp(lamp: Lamp) {
    this.connection.invoke('RegisterLamp', lamp);
  }

  public updateLamp(lamp: Lamp) {
    this.connection.invoke('UpdateLamp', lamp);
  }
}
