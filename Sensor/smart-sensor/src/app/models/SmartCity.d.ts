import Lamp from "./Lamp";

import { PlaceType } from "../enums/PlaceType";

import TrafficSensor from "./TrafficSensor";

import { get } from "http";

export default interface SmartCity {
  Lamp: [{string, Lamp}],
  TotalEnergyUsage: number,
  TotalEnergyNormalUsage: number,
  SavedMoney: number,
  Place: PlaceType
}
