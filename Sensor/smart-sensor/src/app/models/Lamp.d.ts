import { PlaceType } from "../enums/PlaceType";
import TrafficSensor from "./TrafficSensor";

export default interface Lamp {
    Power: number,
    ConditionalPower: number,
    PosX: number,
    PosY: number,
    Place: PlaceType,
    Enabled: boolean,
    WattPower: number,
    TrafficSensor: TrafficSensor,
}