export default interface TrafficSensor {
    Range: number,
    ObjectCount: number
}