import { Component, OnInit } from '@angular/core';
import { ModelDataService } from '../services/model-data.service';
import * as $ from 'jquery';
import SmartCity from '../model/SmartCity';
import { PlaceType } from '../model/PlaceType';
import Lamp from '../model/Lamp';
import Road from '../model/Road';
import Chart from 'chart.js';

@Component({
  selector: 'app-visualization',
  templateUrl: './visualization.component.html',
  styleUrls: ['./visualization.component.scss']
})
export class VisualizationComponent implements OnInit {

  /**
   * Interfejs zapewniający dostęp do własności i metod pozwalających na manipulację wyglądem i prezentacją elementu canvas
   */
  private canvas: HTMLCanvasElement;
  /**
   * Szerokość elementu
   */
  private screenWidth: number;
  /**
   * Wysokość elementu
   */
  private screenHeight: number;
  /**
   * Interfejs dostarczający kontekst renderowania 2D. Służy do rysowania elementów symulacji
   */
  private context: CanvasRenderingContext2D;
  /**
   * Model przechowujący dane symulacji dla rysowanego stanu.
   */
  public model: SmartCity = {} as SmartCity;

  private roads = [
    new Road(1100, 150, 300, 150),
    new Road(1100, 400, 300, 400),
    new Road(300, 150, 300, 650),
    new Road(1100, 150, 1100, 650),
    new Road(700, 150, 700, 400),
    new Road(1100, 650, 300, 650),
    new Road(900, 400, 900, 650),
    new Road(500, 400, 500, 650),
]

  constructor(private modelService: ModelDataService) { }

  ngOnInit() {
    this.canvas = <HTMLCanvasElement>document.getElementById('stage');
    this.screenHeight = 800;
    this.screenWidth = 1280;
    this.context = this.canvas.getContext('2d');
    this.context.imageSmoothingEnabled = false;
    this.setupChart();

    $(() => {
      this.modelService.modelObservable.subscribe(result => {
        if (!result) {
          return;
        }

        this.model = result;
        this.draw();
      });

      //this.setupMouseEvents();
    })
  }

  private setupChart() {
    var canvas = <HTMLCanvasElement>document.getElementById('chart');
    var context = canvas.getContext('2d');
    context.imageSmoothingEnabled = false;
    var chart = new Chart(context, {
      "type": "line", "data": {
        "labels": [],
        "datasets": [{ "label": "Zaoszczędzona energia [%]", "data": [], "fill": false, "borderColor": "rgb(75, 192, 192)", "lineTension": 0.1 }]
      }, "options": {}
    });
    setInterval(() => {
      if (this.model) {
        if (chart.data.labels.length > 50) {
          chart.data.labels.shift();
          chart.data.datasets[0].data.shift();
        }
        chart.data.labels.push("");
        chart.data.datasets[0].data.push(this.model.totalEnergyUsage / this.model.totalEnergyNormalUsage - 100);
        chart.update();
      }
    }, 1000);
  }

    /**
   * Metoda rysująca odpowiednią "klatkę" w zależności od aktualnego modelu symulacji.
   * Główna metoda rysująca, wywołuje metody odpowiedzialne za rysowanie poszczególnych elementów symulacji.
   */
  private draw() {
    // this.context.clearRect(0, 0, this.screenWidth, this.screenHeight);
    this.drawBackground();

    if (this.model) {
      this.drawRoads();
    //  this.drawMovingObjects();
      this.drawLamps();
    }

  //  if (this.setupService.completed)
  //    return;

   // this.drawMouseObjects();
  }

    /**
   * Metoda rysuje odpowiednie tło dla symulacji.
   * Głownym celem jest zamazanie poprzedniej klatki z elementów pozostałych z porzpedniej iteracji systemu.
   */
  private drawBackground() {
    switch (this.model.place) {
      case PlaceType.NormalTraffic:
        this.context.fillStyle = "LightGrey";
        break;
      case PlaceType.HighTraffic:
        this.context.fillStyle = "Beige";
        break;
      case PlaceType.Parks:
        this.context.fillStyle = "LightGreen";
        break;
      default:
        this.context.fillStyle = "White";
        break;
    }

    this.context.fillRect(0, 0, this.screenWidth, this.screenHeight);
  }

  /**
   * Metoda rysuje ścieżki po których poruszają się dynamiczne obiekty.
   */
  private drawRoads() {
    this.roads.forEach((road: Road) => this.drawLine(this.context, road.startX, road.startY, road.endX, road.endY));
  }

  /**
   * Metoda rysuje lampy na mapie symulacji.
   */
  private drawLamps() {
    this.model.lamps.forEach((lamp_dict) => {
      let lamp = lamp_dict.value;
      this.drawRing(this.context, lamp.posX, lamp.posY, 10, lamp.conditionalPower * (lamp.enabled ? 1 : 0) / 100);
    });
  }

  /**
   * Metoda pomocnicza, używana przy rysowaniu ścieżek do poruszania się obiektów.
   * @param context kontekst renderowania
   * @param startX pozycja X początku
   * @param startY pozycja Y początku
   * @param endX pozycja X końca
   * @param endY pozycja Y końca
   */
  private drawLine(context: CanvasRenderingContext2D, startX: number, startY: number, endX: number, endY: number) {
    context.beginPath();
    context.moveTo(startX, startY);
    context.lineWidth = 10;
    context.lineCap = "round";
    context.strokeStyle = "#AAAAAA";
    context.lineTo(endX, endY);
    context.stroke();
  }

  /**
   * Metoda pomocnicza, używana przy rysowaniu lamp.
   * @param context kontekst renderowania
   * @param posX pozycja X
   * @param posY pozycja Y
   * @param radius promień obiektu
   * @param scale współczynnik skali
   * @param innerColor kolor wewnętrznego koła
   * @param outerColor kolor zewnętrznego koła
   */
  private drawRing(context: CanvasRenderingContext2D, posX: number, posY: number, radius: number, scale: number, innerColor: string = 'rgba(0,0,0,0.5)', outerColor: string = 'rgba(255,255,0,0.5)') {
    this.drawCircle(context, posX, posY, radius * 8 * scale, outerColor)
    this.drawCircle(context, posX, posY, radius, innerColor)
  }

  /**
   * Metoda pomocnicza, używana przy rysowaniu lamp.
   * @param context kontekst renderowania
   * @param posX pozycja X
   * @param posY pozycja Y
   * @param radius promień obiektu
   * @param color kolor obiektu
   */
  private drawCircle(context: CanvasRenderingContext2D, posX: number, posY: number, radius: number, color: string = null) {
    context.beginPath();
    context.arc(posX, posY, radius, 0, 2 * Math.PI, false);
    context.closePath();
    if (color) {
      context.fillStyle = color;
      context.fill();
    }
    context.lineWidth = 0;
  }
}
