import { PlaceType } from "./PlaceType";
import TrafficSensor from "./TrafficSensor";

export default interface Lamp {
    power: number,
    conditionalPower: number,
    posX: number,
    posY: number,
    place: PlaceType,
    enabled: boolean,
    wattPower: number,
    trafficSensor: TrafficSensor;
}
