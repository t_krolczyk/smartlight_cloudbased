export default interface TrafficSensor {
  range: number,
  objectCount: number
}
