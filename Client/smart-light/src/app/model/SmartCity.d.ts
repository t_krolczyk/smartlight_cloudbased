import Lamp from "./Lamp";

import { PlaceType } from "./PlaceType";

import TrafficSensor from "./TrafficSensor";

export default interface SmartCity {
  lamps: [{key: string, value: Lamp}],
  totalEnergyUsage: number,
  totalEnergyNormalUsage: number,
  savedMoney: number,
  place: PlaceType
}
