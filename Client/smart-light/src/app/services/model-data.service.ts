import { Injectable } from '@angular/core';
import SmartCity from '../model/SmartCity';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class ModelDataService {

  /**
   * obiekt typu obserwable dla modelu
   */
  modelObservable: BehaviorSubject<SmartCity>;
  /**
   * Instancja modelu
   */
  model: SmartCity;

  constructor() {
    this.modelObservable = new BehaviorSubject<SmartCity>(this.model);
    this.modelObservable.next(this.model);
  }

  /**
   * Metoda służy do zmiany aktualnie przechowywanego modelu symulacji.
   * @param model nowy model symulacji
   */
  setNewModel(model: SmartCity) {
    this.model = model;
    console.log('test');
    this.modelObservable.next(this.model);
  }
}
