import { ModelDataService } from './model-data.service';
import { Injectable } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';

@Injectable({
  providedIn: 'root'
})

export class EndpointService {

  public connection: HubConnection;

  constructor(private modelService: ModelDataService) {
    this.connection = new HubConnectionBuilder()
    //.withUrl('https://localhost:44337/sensor-hub')
    .withUrl('https://smartlight20190606015032.azurewebsites.net:443/sensor-hub')
    .build();

    this.startListening();
  }

  private startListening() {

    this.connection.on('StartListeningResponse', (message) => {
      console.log(message);
    });

    this.connection.on('RegisterLampResponse', (signal) => {
      console.log(signal);
    });

    this.connection.on('SmartCityUpdateResponse', (smartCityModel) => {
      console.log(smartCityModel);
      this.modelService.setNewModel(smartCityModel);
    });

    this.connection.start()
      .then(() => this.connection.invoke('StartListening'));
  }



  // public registerLamp(lamp: Lamp) {
  //   this.connection.invoke('RegisterLamp', lamp);
  // }
}
