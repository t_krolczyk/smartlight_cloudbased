﻿using Microsoft.AspNetCore.SignalR;
using SmartLight.Logic;
using SmartLight.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLight.Hubs
{
    public class SensorHub : Hub
    {
        private readonly Simulation _simulation;

        public SensorHub(Simulation simulation)
        {
            _simulation = simulation;
        } 

        public async Task StartListening()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, "listeners");
            await Clients.Client(Context.ConnectionId).SendAsync("StartListeningResponse", "Server Listening Startred"); 
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            lock (_simulation.SmartCity.Lamps)
            {
                if (_simulation.SmartCity.Lamps.ContainsKey(Context.ConnectionId))
                    _simulation.SmartCity.Lamps.Remove(Context.ConnectionId);
            }

            return base.OnDisconnectedAsync(exception);
        }

        public async Task RegisterLamp(Lamp lamp)
        {
            lock (_simulation.SmartCity.Lamps)
            {
                if (!_simulation.SmartCity.Lamps.ContainsKey(Context.ConnectionId))
                    _simulation.SmartCity.Lamps.Add(Context.ConnectionId, lamp);
            }

            await Groups.AddToGroupAsync(Context.ConnectionId, "lamps");
            await Clients.Client(Context.ConnectionId).SendAsync("RegisterLampResponse", "Lamp " + Context.ConnectionId + " registered.");
        }

        public async Task UpdateLamp(Lamp lamp)
        {
            string message = string.Empty;
            lock (_simulation.SmartCity.Lamps[Context.ConnectionId])
            {
                message = _simulation.UpdateLamp(Context.ConnectionId, lamp); 
            }
            await Clients.Client(Context.ConnectionId).SendAsync("UpdateLampResponse", message);
        }
    }
}
