﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLight.Enums
{
    public enum PlaceType
    {
        NormalTraffic,
        HighTraffic,
        DangerousPlace,
        Park,
        Pedestrian
    }
}
