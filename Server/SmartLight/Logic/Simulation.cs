﻿using Microsoft.AspNetCore.SignalR;
using SmartLight.Enums;
using SmartLight.Hubs;
using SmartLight.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace SmartLight.Logic
{
    public class Simulation
    {
        private IEnumerable<PropertyInfo> LampProperties { get; set; }
        private IEnumerable<PropertyInfo> TrafficSensorProperties { get; set; }
        private IHubContext<SensorHub> Context { get; set; }

        private int Delay { get; set; }
        public SmartCity SmartCity { get; set; }

        public Simulation(IHubContext<SensorHub> hubContext)
        {
            Context = hubContext;
            Delay = 1000;
            LampProperties = typeof(Lamp).GetProperties().Where(p => p.Name != "ConditionalPower");
            TrafficSensorProperties = typeof(TrafficSensor).GetProperties();

            SmartCity = new SmartCity
            {
                Lamps = new Dictionary<string, Lamp>(),
                Place = PlaceType.NormalTraffic
            };

            new Task(StartSimulation).Start();
        }        

        public void StartSimulation()
        {
            SmartCity processingModel;
            while(true)
            {
                lock (SmartCity)
                {
                    processingModel = CreateProcessingModel();
                }

                CalculatePowerUsage(processingModel);

                lock (SmartCity)
                {
                    SmartCity.TotalEnergyNormalUsage = processingModel.TotalEnergyNormalUsage;
                    SmartCity.TotalEnergyUsage = processingModel.TotalEnergyUsage;
                }

                Context.Clients.Group("listeners").SendAsync("SmartCityUpdateResponse", SmartCity);
                Thread.Sleep(Delay);
            }
        }

        private SmartCity CreateProcessingModel()
        {
            return new SmartCity
            {
                Lamps = SmartCity.Lamps.Select(l => new { l.Key, Value = l.Value.Clone() }).ToDictionary(l => l.Key, l => l.Value),
                Place = SmartCity.Place,
                SavedMoney = SmartCity.SavedMoney,
                TotalEnergyNormalUsage = SmartCity.TotalEnergyNormalUsage,
                TotalEnergyUsage = SmartCity.TotalEnergyUsage,
            };
        }

        public string UpdateLamp(string connectionId, Lamp lamp)
        {            
            List<string> messages = new List<string>();
            Lamp before = SmartCity.Lamps[connectionId];

            foreach(PropertyInfo prop in LampProperties)
            {
                if (prop.GetValue(before)?.ToString() != prop.GetValue(lamp)?.ToString())
                {
                    messages.Add(prop.Name + " - before: " + prop.GetValue(before) + ", after: " + prop.GetValue(lamp));
                    prop.SetValue(before, prop.GetValue(lamp));
                }
            }

            foreach (PropertyInfo prop in TrafficSensorProperties)
            {
                if (prop.GetValue(before.TrafficSensor)?.ToString() != prop.GetValue(lamp.TrafficSensor)?.ToString())
                {
                    messages.Add(prop.Name + " - before: " + prop.GetValue(before.TrafficSensor) + ", after: " + prop.GetValue(lamp.TrafficSensor));
                    prop.SetValue(before.TrafficSensor, prop.GetValue(lamp.TrafficSensor));
                }
            }

            CalculateLampConditionalPower(before);

            return $"Lamp {connectionId} updated: {string.Join("\n\r", messages)}";
                       
        }

        private void CalculatePowerUsage(SmartCity processingModel)
        {
            foreach(Lamp lamp in processingModel.Lamps.Values)
            {
                processingModel.TotalEnergyNormalUsage += lamp.WattPower * (decimal)Delay / 1000m;
                processingModel.TotalEnergyUsage += lamp.ConditionalPower * lamp.WattPower * (decimal)Delay / 1000m;
            }
        }

        private void CalculateLampConditionalPower(Lamp lamp)
        {
            decimal powerRatio = 1;

            if (lamp.TrafficSensor.ObjectCount >= 8)
            {
                powerRatio = 1.75m;
            }
            if (lamp.TrafficSensor.ObjectCount >= 5)
            {
                powerRatio = 1.5m;
            }
            if (lamp.TrafficSensor.ObjectCount >= 2)
            {
                powerRatio = 1.25m;
            }

            lamp.ConditionalPower = lamp.Power * powerRatio;
        }

    }
}
