﻿namespace SmartLight.Model
{
    public interface ICloneable<T>
    {
        T Clone();
    }
}