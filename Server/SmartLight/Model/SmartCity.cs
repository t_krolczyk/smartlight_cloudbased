﻿using Newtonsoft.Json;
using SmartLight.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq;

namespace SmartLight.Model
{
    public class SmartCity
    {
        [JsonIgnore]
        public IDictionary<string, Lamp> Lamps { get; set; }

        [JsonProperty(PropertyName = "lamps")]
        public object JSLamps => Lamps.Select(l => new { l.Key, l.Value });
        public decimal TotalEnergyUsage { get; set; }
        public decimal TotalEnergyNormalUsage { get; set; }
        public decimal SavedMoney { get; set; }
        public PlaceType Place { get; set; }
    }
}
