﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLight.Model
{
    public class TrafficSensor
    {
        public decimal Range { get; set; }
        public int ObjectCount { get; set; }
    }
}
