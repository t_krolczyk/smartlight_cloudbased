﻿
using SmartLight.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartLight.Model
{
    public class Lamp : ICloneable<Lamp>
    {
        public decimal Power { get; set; }
        public decimal ConditionalPower { get; set; }
        public decimal PosX { get; set; }
        public decimal PosY { get; set; }
        public PlaceType PlaceType { get; set; }
        public bool Enabled { get; set; }
        public decimal WattPower { get; set; }
        public TrafficSensor TrafficSensor { get; set; }

        public Lamp Clone()
        {
            return new Lamp
            {
                ConditionalPower = ConditionalPower,
                Enabled = Enabled,
                PlaceType = PlaceType,
                PosX = PosX,
                PosY = PosY,
                Power = Power,
                TrafficSensor = new TrafficSensor
                {
                    ObjectCount = TrafficSensor.ObjectCount,
                    Range = TrafficSensor.Range,
                },
                WattPower = WattPower,
            };
        }
    }
}
